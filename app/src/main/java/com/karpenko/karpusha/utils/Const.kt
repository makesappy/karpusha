package com.karpenko.karpusha.utils

object Const {
    // Get them from Telegram's API console
    const val API_ID = ConstUntracked.API_ID
    const val API_HASH = ConstUntracked.API_HASH

    const val APP_VERSION = "AppVersion"
    const val MODEL = "Model"
    const val SYSTEM_VERSION = "SysVer"
    const val LANG_CODE = "ru"

    const val EXTRA_PHONE_HASH = "phoneHash"
    const val EXTRA_PHONE_NUMBER = "phoneNumber"
}