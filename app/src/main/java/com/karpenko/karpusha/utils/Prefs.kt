package com.karpenko.karpusha.utils

import android.app.Activity
import android.content.Context
import android.content.SharedPreferences

class Prefs(private val ctx: Context) {
    companion object{
        const val PREFS = "AppPrefs"
        const val PREFS_AUTHENTICATED_KEY = "h673gft"
    }

    fun setAuthenticated(b:Boolean){
        setBoolean(PREFS_AUTHENTICATED_KEY,b)
    }

    fun isAuthenticated():Boolean{
        return getBoolean(PREFS_AUTHENTICATED_KEY)
    }

    private fun getPrefs(): SharedPreferences {
        return ctx.getSharedPreferences(PREFS, Activity.MODE_PRIVATE)
    }
    fun setString(key:String, value:String){
        getPrefs().edit().putString(key,value).apply()
    }

    fun getString(key: String): String? {
        return getPrefs().getString(key, null);
    }

    fun setBoolean(key:String, value:Boolean){
        getPrefs().edit().putBoolean(key,value).apply()
    }

    fun getBoolean(key: String): Boolean {
        return getPrefs().getBoolean(key, false)
    }
}