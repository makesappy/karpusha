package com.karpenko.karpusha.data

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.karpenko.karpusha.data.db.PushesDao
import com.karpenko.karpusha.data.db.model.Push

@Database(
    entities = [Push::class],
    version = 1
)
abstract class AppDatabase :RoomDatabase() {
    abstract fun getDao(): PushesDao

    companion object {
        @Volatile private var instance: AppDatabase? = null
        private val LOCK = Any()

        operator fun invoke(context: Context)= instance ?: synchronized(LOCK){
            instance ?: buildDatabase(context).also { instance = it}
        }

        private fun buildDatabase(context: Context) = Room.databaseBuilder(context,
            AppDatabase::class.java, "pushes.db")
            .build()
    }
}