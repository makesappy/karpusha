package com.karpenko.karpusha.data.db

import androidx.lifecycle.LiveData
import androidx.room.*
import com.karpenko.karpusha.data.db.model.Push

@Dao
interface PushesDao {
    @Query("SELECT * FROM pushes")
    fun getAll(): LiveData<List<Push>>

    @Insert
    fun insertAll(vararg pushes: Push)

    @Insert
    fun insertAll(pushes: List<Push>)

    @Delete
    fun delete(push:Push)

    @Query("DELETE FROM pushes")
    fun deleteAll()

    @Update
    fun update(vararg pushes: Push)
}