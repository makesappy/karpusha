package com.karpenko.karpusha.data.db.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "pushes")
data class Push(
    @ColumnInfo(name = "imgUrl")
    var imgUrl: String,
    @ColumnInfo(name = "body")
    var body: String
){
    @PrimaryKey(autoGenerate = true)
    var id: Int = 0
}