package com.karpenko.karpusha.data.tg

import com.github.badoualy.telegram.api.TelegramApiStorage
import com.github.badoualy.telegram.mtproto.auth.AuthKey
import com.github.badoualy.telegram.mtproto.model.DataCenter
import com.github.badoualy.telegram.mtproto.model.MTSession
import org.apache.commons.io.FileUtils
import java.io.File
import java.io.FileNotFoundException
import java.io.IOException

class TgApiStorage : TelegramApiStorage {
    private val authKeyFile: File = File("Properties/auth.key")
    private val nearestDcFile: File = File("Properties/dc.save")

    override fun saveAuthKey(authKey: AuthKey) = authKeyFile.tryWrite {
        it.writeBytes(authKey.key)
    }

    override fun loadAuthKey() = authKeyFile.tryRead {
        AuthKey(it.readBytes())
    }

    override fun saveDc(dataCenter: DataCenter) = nearestDcFile.tryWrite {
        it.writeText(dataCenter.toString())
    }

    override fun loadDc() = nearestDcFile.tryRead {
        val fields = it.readText().split(":")
        DataCenter(fields[0], fields[1].toInt())
    }

    override fun deleteAuthKey() = authKeyFile.tryWrite { FileUtils.forceDelete(it) }

    override fun deleteDc() = nearestDcFile.tryWrite { FileUtils.forceDelete(it) }

    override fun saveSession(session: MTSession?) {

    }

    override fun loadSession(): Nothing? = null

    private fun <T> File.tryRead(block: (File) -> T): T? {
        try {
            return block.invoke(this)
        } catch (e: IOException) {
            if (e !is FileNotFoundException)
                e.printStackTrace()
        }
        return null
    }

    private fun File.tryWrite(block: (File) -> Unit) {
        try {
            block.invoke(this)
        } catch (e: IOException) {
            e.printStackTrace()
        }
    }
}