package com.karpenko.karpusha.core

import android.app.Application
import androidx.appcompat.app.AppCompatDelegate
import com.facebook.stetho.Stetho
import com.github.badoualy.telegram.api.TelegramApp
import com.karpenko.karpusha.data.AppDatabase
import com.karpenko.karpusha.utils.Const
import com.karpenko.karpusha.utils.Prefs
import org.koin.android.ext.android.getKoin
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.Koin
import org.koin.core.context.startKoin
import org.koin.dsl.module

class App :Application() {
    var prefs = Prefs(this)

    private val appModule = module {
        single{ TelegramApp(
            Const.API_ID,
            Const.API_HASH,
            Const.MODEL,
            Const.SYSTEM_VERSION,
            Const.APP_VERSION,
            Const.LANG_CODE
        ) }
        single { AppDatabase(this@App) }
    }
    private lateinit var appKoin : Koin

    companion object{
        lateinit var instance:App
    }

    override fun onCreate() {
        super.onCreate()

        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)

        instance = this

        Stetho.initializeWithDefaults(this);

        startKoin {
            androidLogger()
            androidContext(this@App)
            modules(appModule)
            appKoin = getKoin()
        }
    }


}