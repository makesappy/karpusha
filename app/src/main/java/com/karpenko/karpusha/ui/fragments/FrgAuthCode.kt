package com.karpenko.karpusha.ui.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.karpenko.karpusha.R
import com.karpenko.karpusha.utils.Const
import kotlinx.android.synthetic.main.frg_auth_code.*

class FrgAuthCode :FrgBase(){
    companion object{
        fun newInstance(phoneHash: String,phoneNumber:String): FrgAuthCode {
            val args = Bundle()
            args.putString(Const.EXTRA_PHONE_HASH,phoneHash)
            args.putString(Const.EXTRA_PHONE_NUMBER,phoneNumber)
            val frg = FrgAuthCode()
            frg.arguments = args
            return frg
        }
    }

    private lateinit var phoneHash: String
    private lateinit var phoneNumber: String

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.frg_auth_code,container,false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        phoneHash = arguments?.getString(Const.EXTRA_PHONE_HASH,null)?:""
        phoneNumber = arguments?.getString(Const.EXTRA_PHONE_NUMBER,null)?:""

        bEnterPassword.setOnClickListener {
//            val client = Kotlogram.getDefaultClient(tgApp, TgApiStorage())
//            val authorization =
//                try {
//                    client.authSignIn(phoneNumber, phoneHash, etAuthCode.text.toString())
//                } catch (e: RpcErrorException) {
//                    if (e.tag.equals("SESSION_PASSWORD_NEEDED", true)) {
//                        openFrg(FrgEnterPassword.newInstance())
//                    } else throw e
//                }

            replaceFrg(FrgEnterPassword.newInstance())
        }
    }
}