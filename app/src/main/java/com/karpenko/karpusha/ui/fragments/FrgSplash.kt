package com.karpenko.karpusha.ui.fragments

import android.graphics.PorterDuff
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AccelerateInterpolator
import android.view.animation.AlphaAnimation
import android.view.animation.AnimationSet
import android.view.animation.DecelerateInterpolator
import android.widget.ImageView
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import com.karpenko.karpusha.R
import com.karpenko.karpusha.core.App
import kotlinx.android.synthetic.main.frg_splash.*
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch


class FrgSplash : FrgBase() {
    companion object {
        fun newInstance(): FrgSplash {
            return FrgSplash()
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.frg_splash, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
    }

    private fun initView() {
        uiScope.launch(exceptionHandler) {
            delay(300)
            imageViewAnimatedChange(
                ivIconSplash,
                ResourcesCompat.getDrawable(act.resources, R.drawable.ic_ball, act.theme),
                R.color.color1SplashIconBg
            )
            delay(1000)
            imageViewAnimatedChange(
                ivIconSplash,
                ResourcesCompat.getDrawable(act.resources, R.drawable.ic_cap, act.theme),
                R.color.color2SplashIconBg
            )
            delay(1000)
            imageViewAnimatedChange(
                ivIconSplash,
                ResourcesCompat.getDrawable(act.resources, R.drawable.ic_gloves, act.theme),
                R.color.color3SplashIconBg
            )
            delay(700)
            imageViewAnimatedChange(ivIconSplash, null, -1)
            delay(200)
            if (!App.instance.prefs.isAuthenticated()) {
                replaceFrg(FrgSignIn.newInstance())
            } else {
                replaceFrg(FrgListOfPushes.newInstance())
            }
        }
    }

    private fun imageViewAnimatedChange(iv: ImageView?, newImage: Drawable?, colorResId: Int) {
        iv?.let {
            newImage?.setColorFilter(ContextCompat.getColor(act, colorResId), PorterDuff.Mode.SRC_ATOP)
            it.setImageDrawable(newImage)
            val fadeIn = AlphaAnimation(0f, 1f)
            fadeIn.interpolator = DecelerateInterpolator()
            fadeIn.duration = 1000

            val fadeOut = AlphaAnimation(1f, 0f)
            fadeOut.interpolator = AccelerateInterpolator()
            fadeOut.startOffset = 1000
            fadeOut.duration = 1000

            val animation = AnimationSet(false)
            animation.addAnimation(fadeIn)
            animation.addAnimation(fadeOut)
            it.animation = animation
            animation.start()
        }
    }
}