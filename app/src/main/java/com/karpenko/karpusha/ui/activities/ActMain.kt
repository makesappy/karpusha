package com.karpenko.karpusha.ui.activities

import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import com.karpenko.karpusha.R
import com.karpenko.karpusha.ui.fragments.FrgSplash

class ActMain : ActBase(){
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        openFrg(FrgSplash.newInstance())
    }
}
