package com.karpenko.karpusha.ui.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.karpenko.karpusha.R
import kotlinx.android.synthetic.main.frg_enter_password.*

class FrgEnterPassword :FrgBase(){
    companion object{
        fun newInstance(): FrgEnterPassword{
            return FrgEnterPassword()
        }
    }
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.frg_enter_password,container,false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        bGoToClient.setOnClickListener {
            replaceFrg(FrgListOfPushes.newInstance())
        }
    }
}