package com.karpenko.karpusha.ui.viewModel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class SignInViewModel(phoneNumber : String) :ViewModel(){
    val phoneText :MutableLiveData<String> = MutableLiveData()
    val signInBtnAvailability :MutableLiveData<Boolean> = MutableLiveData()

    init {
        phoneText.value = phoneNumber
        signInBtnAvailability.value = false
    }

    @Suppress("UNCHECKED_CAST")
    class SignInViewModelFactory(private var phoneNumber : String): ViewModelProvider.Factory {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            return SignInViewModel(phoneNumber) as T
        }
    }
}