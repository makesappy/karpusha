package com.karpenko.karpusha.ui.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.karpenko.karpusha.R
import com.karpenko.karpusha.core.App
import com.karpenko.karpusha.ui.adapters.PushesListAdapter
import com.karpenko.karpusha.ui.viewModel.PushesListViewModel
import kotlinx.android.synthetic.main.frg_list_of_pushes.*
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class FrgListOfPushes :FrgBase(){
    companion object{
        fun newInstance(): FrgListOfPushes {
            return FrgListOfPushes()
        }
    }

    private lateinit var viewModel: PushesListViewModel
    private lateinit var pushesAdapter: PushesListAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.frg_list_of_pushes,container,false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        App.instance.prefs.setAuthenticated(true)

        initView()

        initViewModel()

        initViewListeners()
    }

    private fun initView() {
        pushesAdapter = PushesListAdapter()
        rvListOfPushes.layoutManager = LinearLayoutManager(act)
        rvListOfPushes.adapter = pushesAdapter
    }

    private fun initViewModel() {
        viewModel = ViewModelProviders.of(this, PushesListViewModel.PushesListViewModelFactory(db))
            .get(PushesListViewModel::class.java)

        viewModel.pushesLiveData.observe(this, Observer {
            pushesAdapter.pushes = it
            pushesAdapter.notifyDataSetChanged()
        })
    }

    private fun initViewListeners() {
        swipeRefreshLayout.setOnRefreshListener {
            uiScope.launch(exceptionHandler) {
                viewModel.fetchAndShowPushes()
                delay(2000)
                swipeRefreshLayout.isRefreshing = false
            }
        }
        fab.setOnClickListener {
            viewModel.deleteAllFromDb()
        }
        fab.setOnLongClickListener {
            viewModel.setPrefsUnLogged()
            replaceFrg(FrgSignIn.newInstance())
            return@setOnLongClickListener true
        }
    }
}