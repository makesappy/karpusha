package com.karpenko.karpusha.ui.activities

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import com.karpenko.karpusha.R

abstract class ActBase :AppCompatActivity(){

    fun replaceFrg(frg: Fragment) {
        supportFragmentManager
            .beginTransaction()
            .replace(R.id.frgContainer,frg)
            .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
            .commit()
        supportFragmentManager.popBackStack()
    }
    fun closeFrg(frg: Fragment) {
        supportFragmentManager
            .beginTransaction()
            .remove(frg)
            .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE)
            .commit()
        supportFragmentManager.popBackStack()
    }
    fun openFrg(frg: Fragment) {
        supportFragmentManager
            .beginTransaction()
            .add(R.id.frgContainer,frg)
            .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
            .commit()
        supportFragmentManager.popBackStack()
    }
}