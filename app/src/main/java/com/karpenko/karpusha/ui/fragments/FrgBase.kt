package com.karpenko.karpusha.ui.fragments

import android.content.Context
import androidx.fragment.app.Fragment
import com.github.badoualy.telegram.api.TelegramApp
import com.karpenko.karpusha.data.AppDatabase
import com.karpenko.karpusha.ui.UiLifecycleScope
import com.karpenko.karpusha.ui.activities.ActMain
import kotlinx.coroutines.CoroutineExceptionHandler
import org.koin.android.ext.android.inject
import timber.log.Timber

abstract class FrgBase :Fragment(){
    lateinit var act :ActMain
    val tgApp: TelegramApp by inject()
    val db: AppDatabase by inject()
    protected val uiScope = UiLifecycleScope()
    protected val exceptionHandler = CoroutineExceptionHandler { _, throwable ->
        Timber.tag(javaClass.canonicalName).e(throwable)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        act = context as ActMain
        lifecycle.addObserver(uiScope)
    }

    override fun onDetach() {
        super.onDetach()
        lifecycle.removeObserver(uiScope)
    }

    protected fun openFrg(frg: Fragment){
        act.openFrg(frg)
    }

    protected fun replaceFrg(frg: Fragment){
        act.replaceFrg(frg)
    }

    protected fun finishFrg(){
        act.closeFrg(this)
    }
}