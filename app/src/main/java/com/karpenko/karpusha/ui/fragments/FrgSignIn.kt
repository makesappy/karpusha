package com.karpenko.karpusha.ui.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.widget.doOnTextChanged
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.karpenko.karpusha.R
import com.karpenko.karpusha.ui.viewModel.SignInViewModel
import kotlinx.android.synthetic.main.frg_sign_in.*

class FrgSignIn :FrgBase() {

    companion object{
        fun newInstance(): FrgSignIn{
            return FrgSignIn()
        }
    }

    private lateinit var viewModel :SignInViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.frg_sign_in,container,false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initViewModel()

        initViewListeners()
    }

    private fun initViewListeners() {
        bSendCode.setOnClickListener {
            //            val client = Kotlogram.getDefaultClient(tgApp, TgApiStorage())
    //            val sentCode = client.authSendCode(false, etPhone.text.toString(), true)

            replaceFrg(FrgAuthCode.newInstance("", ""))
        }
        etPhone.doOnTextChanged { text, _, _, _ -> viewModel.phoneText.postValue(text.toString()) }
    }

    private fun initViewModel() {
        viewModel = ViewModelProviders.of(
            this,
            SignInViewModel.SignInViewModelFactory(etPhone.text.toString())
        )
            .get(SignInViewModel::class.java)

        viewModel.phoneText.observe(this, Observer {
            viewModel.signInBtnAvailability.postValue(it.toString().length == 13)
        })
        viewModel.signInBtnAvailability.observe(this, Observer {
            bSendCode.isEnabled = it
        })
    }
}