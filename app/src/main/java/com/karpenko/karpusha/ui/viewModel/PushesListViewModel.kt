package com.karpenko.karpusha.ui.viewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import com.karpenko.karpusha.core.App
import com.karpenko.karpusha.data.AppDatabase
import com.karpenko.karpusha.data.db.model.Push
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import timber.log.Timber

class PushesListViewModel(private val db: AppDatabase) : ViewModel(){
    var pushesLiveData: LiveData<List<Push>> = db.getDao().getAll()
    private val handler = CoroutineExceptionHandler { _, throwable ->
        Timber.tag(javaClass.canonicalName).e(throwable)
    }

    fun fetchAndShowPushes(){
        viewModelScope.launch(handler) {
            val pushes = ArrayList<Push>()

            //Dummy data
            pushes.add(Push("https://img.freepik.com/free-vector/doodle-soccer-ball_1034-741.jpg?size=338&ext=jpg",
                "Lorem vfvdfv ispsovosfv"))
            pushes.add(Push("https://img.freepik.com/free-vector/doodle-soccer-ball_1034-741.jpg?size=338&ext=jpg",
                "Lorem ispsovsvfsfvosfv"))
            pushes.add(Push("https://img.freepik.com/free-vector/doodle-soccer-ball_1034-741.jpg?size=338&ext=jpg",
                "Loresvfsfvm ispsovosfv"))
            pushes.add(Push("https://img.freepik.com/free-vector/doodle-soccer-ball_1034-741.jpg?size=338&ext=jpg",
                "Lsvfsfvorem ispsovosfv"))
            pushes.add(Push("https://img.freepik.com/free-vector/doodle-soccer-ball_1034-741.jpg?size=338&ext=jpg",
                "Lorem ispsovosvsfvsffv"))

            withContext(Dispatchers.IO){
                db.getDao().insertAll(pushes)
            }
        }
    }

    fun deleteAllFromDb(){
        viewModelScope.launch {
            withContext(Dispatchers.IO){
                db.getDao().deleteAll()
            }
        }
    }

    fun setPrefsUnLogged() {
        App.instance.prefs.setAuthenticated(false)
    }

    @Suppress("UNCHECKED_CAST")
    class PushesListViewModelFactory(private val db: AppDatabase) : ViewModelProvider.Factory {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            return PushesListViewModel(db) as T
        }
    }
}