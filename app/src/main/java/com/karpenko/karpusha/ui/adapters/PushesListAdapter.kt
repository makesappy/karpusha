package com.karpenko.karpusha.ui.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.karpenko.karpusha.R
import com.karpenko.karpusha.data.db.model.Push
import kotlinx.android.synthetic.main.push_item.view.*

class PushesListAdapter :
    RecyclerView.Adapter<PushesListAdapter.PushesListViewHolder>() {

    var pushes :List<Push> = emptyList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PushesListViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.push_item, parent, false)
        return PushesListViewHolder(v)
    }

    override fun getItemCount(): Int {
        return pushes.size
    }

    override fun onBindViewHolder(holder: PushesListViewHolder, position: Int) {
        holder.bindView(pushes[position])
    }

    class PushesListViewHolder(private val mItemView: View) : RecyclerView.ViewHolder(mItemView){
        fun bindView(push: Push) {
            Glide.with(itemView).load(push.imgUrl).into(mItemView.ivPushItemType)
            mItemView.tvPushDesc.text = push.body
        }
    }
}